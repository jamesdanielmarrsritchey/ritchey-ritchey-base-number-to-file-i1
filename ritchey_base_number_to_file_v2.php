<?php
#Name:Ritchey Base Number To File v2
#Description:Convert a Ritchey Base Number string to a file using the Ritchey Base Number decoding scheme. Returns "TRUE" on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'number' is a Ritchey Base Number. 'destination' (required) is a path to write the file at. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):number:number:required,destination:file:required,display_errors:bool:optional
#Content:
if (function_exists('ritchey_base_number_to_file_v2') === FALSE){
function ritchey_base_number_to_file_v2($number, $destination, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if (@is_dir(dirname($destination)) === FALSE){
		$errors[] = "destination";
	}
	if (@ctype_digit($number) === FALSE){
		$errors[] = "number - not a number";
	}
	###Ensure the number is even, because Ritchey Base Numbers are always even!
	if (@strpbrk(substr(strlen($number), -1), 13579) == TRUE){
		$errors[] = "number - odd number";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Convert number to an array of 2 digit sets. Change each 2 digit set into a Base64 character. Convert array to string. Decode Base64. Write data to destination. NOTE: Base64 can encode/decode in chunks of 3 bytes and still achieve the same result as processing data all at once, but this implementation does not make use of this. There is no point since it receives the number as a variable not in a file.]
	if (@empty($errors) === TRUE){
		###Convert number to an array of 2 digit sets
		$number = @str_split($number, 2);
		###Convert each number set to a base64 character.
		foreach ($number as &$character){
			if ($character === "10"){
				$character = 'A';	
			} else if ($character === "11"){
				$character = 'B';
			} else if ($character === "12"){
				$character = 'C';
			} else if ($character === "13"){
				$character = 'D';
			} else if ($character === "14"){
				$character = 'E';
			} else if ($character === "15"){
				$character = 'F';
			} else if ($character === "16"){
				$character = 'G';
			} else if ($character === "17"){
				$character = 'H';
			} else if ($character === "18"){
				$character = 'I';	
			} else if ($character === "19"){
				$character = 'J';	
			} else if ($character === "20"){
				$character = 'K';	
			} else if ($character === "21"){
				$character = 'L';	
			} else if ($character === "22"){
				$character = 'M';	
			} else if ($character === "23"){
				$character = 'N';	
			} else if ($character === "24"){
				$character = 'O';	
			} else if ($character === "25"){
				$character = 'P';	
			} else if ($character === "26"){
				$character = 'Q';	
			} else if ($character === "27"){
				$character = 'R';	
			} else if ($character === "28"){
				$character = 'S';	
			} else if ($character === "29"){
				$character = 'T';	
			} else if ($character === "30"){
				$character = 'U';	
			} else if ($character === "31"){
				$character = 'V';	
			} else if ($character === "32"){
				$character = 'W';	
			} else if ($character === "33"){
				$character = 'X';	
			} else if ($character === "34"){
				$character = 'Y';	
			} else if ($character === "35"){
				$character = 'Z';	
			} else if ($character === "36"){
				$character = 'a';	
			} else if ($character === "37"){
				$character = 'b';
			} else if ($character === "38"){
				$character = 'c';
			} else if ($character === "39"){
				$character = 'd';
			} else if ($character === "40"){
				$character = 'e';
			} else if ($character === "41"){
				$character = 'f';
			} else if ($character === "42"){
				$character = 'g';
			} else if ($character === "43"){
				$character = 'h';
			} else if ($character === "44"){
				$character = 'i';	
			} else if ($character === "45"){
				$character = 'j';	
			} else if ($character === "46"){
				$character = 'k';	
			} else if ($character === "47"){
				$character = 'l';	
			} else if ($character === "48"){
				$character = 'm';	
			} else if ($character === "49"){
				$character = 'n';	
			} else if ($character === "50"){
				$character = 'o';	
			} else if ($character === "51"){
				$character = 'p';	
			} else if ($character === "52"){
				$character = 'q';	
			} else if ($character === "53"){
				$character = 'r';	
			} else if ($character === "54"){
				$character = 's';	
			} else if ($character === "55"){
				$character = 't';	
			} else if ($character === "56"){
				$character = 'u';	
			} else if ($character === "57"){
				$character = 'v';	
			} else if ($character === "58"){
				$character = 'w';	
			} else if ($character === "59"){
				$character = 'x';	
			} else if ($character === "60"){
				$character = 'y';	
			} else if ($character === "61"){
				$character = 'z';	
			} else if ($character === "62"){
				$character = '0';	
			} else if ($character === "63"){
				$character = '1';	
			} else if ($character === "64"){
				$character = '2';	
			} else if ($character === "65"){
				$character = '3';	
			} else if ($character === "66"){
				$character = '4';	
			} else if ($character === "67"){
				$character = '5';	
			} else if ($character === "68"){
				$character = '6';	
			} else if ($character === "69"){
				$character = '7';	
			} else if ($character === "70"){
				$character = '8';	
			} else if ($character === "71"){
				$character = '9';	
			} else if ($character === "72"){
				$character = '+';	
			} else if ($character === "73"){
				$character = '/';	
			} else if ($character === "74"){
				$character = '=';	
			} else {
				$errors[] = "task - Convert numbers to characters";
				goto result;
			}
		}
		###Convert array back to string.
		$number = @implode($number);
		###Decode base64
		$number = @base64_decode($number);
		###Save data to destination
		@file_put_contents($destination, $number);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('ritchey_base_number_to_file_v2_format_error') === FALSE){
			function ritchey_base_number_to_file_v2_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("ritchey_base_number_to_file_v2_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return TRUE;
	} else {
		return FALSE;
	}
}
}
?>